# Changelog

L'ensemble des changements réalisés sur ce projet doivent être documentés dans ce fichier.

Son format est basé sur [Keep a Changelog](https://keepachangelog.com/fr/1.1.0/),
et le versionning respecte la [gestion sémantique de version](https://semver.org/spec/v2.0.0.html) avec la conservation des mots clés en anglais.

**Ce dépôt fonctionne sans tag. On utilisera uniquement la date en titre de section.**

## [Unreleased]

## 2024-07-30

### Added

- Possibilité de modifier le *host port*

### Fixed

- Docker - Retrait de l'attribut déprécié `version:` des fichiers `docker-compose.yml`
- Changelog - Corrections mineures

## 2023-09-12

### Added

- [CHANGELOG](/CHANGELOG.md) - Initialisation
- [README](/README.md) - Initialisation

### Changed

- Suppression des anciennes variables d'environnement de [.env.example](/.env.example) pour ne garder que l'essentiel
- [docker-compose.yml](/docker-compose.yml) - Intégration des nouvelles variables d'environnement (`CONTAINER_NAME`, `DATA_PATH`)
- [.gitignore](/.gitignore) - Ajout de la possibilité de surcharger le fichier [docker-compose.yml](/docker-compose.yml) par un fichier `docker-compose.override.yml` non suivi
