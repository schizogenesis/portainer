# Portainer

Portainer est une WebUI de management Docker particulièrement intuitive.

## Installation

Le container peut être lancé en l'état à l'aide de la commande `docker compose`.

```bash
docker compose up -d
```

Cependant, vous pouvez adapter le container à vos besoin :

- en copiant le fichier [.env.example](/.env.example) en `.env` et en modifiant les variables d'environnement.

```bash
cp .env.example .env
```

- en créant un fichier `docker-compose.override.yml` et en y surchargeant ce que vous voulez.

## Utilisation

### En local

En local, portainer est accessible depuis : [http://localhost:9000](http://localhost:9000)

### Sur un serveur distant

Sur un serveur distant, il faut utiliser l'adresse IP de la machine : `http://<IP_ADDRESS>:9000`.
